package com.rikkeisoft.demoroomdatabase.helper;

import com.rikkeisoft.demoroomdatabase.listener.CameraListener;

/**
 * Created by Administrator on 29/06/2017.
 */

public class CameraHelper {
    CameraListener cameraListener;
    public CameraHelper(CameraListener cameraListener){
        this.cameraListener = cameraListener;
    }
    public void startcameraAction(){
        cameraListener.startProcess();
    }
    public void processCamera(){
        cameraListener.processCam();
    }
    public void processGalery(){
        cameraListener.processGa();
    }
}
