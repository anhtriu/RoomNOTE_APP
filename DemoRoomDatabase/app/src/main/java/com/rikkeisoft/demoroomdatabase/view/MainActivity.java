package com.rikkeisoft.demoroomdatabase.view;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.rikkeisoft.demoroomdatabase.R;
import com.rikkeisoft.demoroomdatabase.adapter.RecylerNoteAdapter;
import com.rikkeisoft.demoroomdatabase.entity.Note;
import com.rikkeisoft.demoroomdatabase.viewmodel.NoteListViewModel;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements LifecycleRegistryOwner, View.OnClickListener {
    private NoteListViewModel viewModel;
    private RecylerNoteAdapter recylerNoteAdapter;
    private RecyclerView rvNotes;
    public ArrayList<Note> arrNotes;
    private Toolbar tb_mainToolbar;
    private  LifecycleRegistry mRegistry;
    public static final int REQUEST_CODEE = 1995;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        setupToolbar();
    }

    private void setupToolbar() {
        setSupportActionBar(tb_mainToolbar);
        getSupportActionBar().setTitle("Note");
        getSupportActionBar().setIcon(R.drawable.note);
    }

    private void init() {
        tb_mainToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        mRegistry = new LifecycleRegistry(this);
        arrNotes = new ArrayList<>();
        viewModel = new NoteListViewModel(this.getApplication());
        rvNotes = (RecyclerView) findViewById(R.id.listNote);
        recylerNoteAdapter = new RecylerNoteAdapter(getApplicationContext(),arrNotes,this);
        rvNotes.setLayoutManager(new GridLayoutManager(this,2));
        rvNotes.hasFixedSize();
        rvNotes.setAdapter(recylerNoteAdapter);
        recylerNoteAdapter.notifyDataSetChanged();
        viewModel = ViewModelProviders.of(this).get(NoteListViewModel.class);
        viewModel.getListNoteModel().observe(MainActivity.this, new Observer<List<Note>>() {
            @Override
            public void onChanged(@Nullable List<Note> notes) {
                recylerNoteAdapter.addNotesItem(notes);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.mn_add:
                Intent o = new Intent(this,AddActivity.class);
                o.putExtra("LENGTH",arrNotes.size());
                startActivity(o);

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public LifecycleRegistry getLifecycle() {
        return mRegistry;
    }

    @Override
    public void onClick(View v) {
        Note note = (Note) v.getTag();
        Integer i = (Integer) v.getTag(R.string.pos);
        Intent intent = new Intent(MainActivity.this,NoteDetailActivity.class);
        intent.putExtra("NOTE",note);
        intent.putExtra("POS",i);
        intent.putExtra("LISTNOTE",arrNotes);
        startActivity(intent);
    }
}
