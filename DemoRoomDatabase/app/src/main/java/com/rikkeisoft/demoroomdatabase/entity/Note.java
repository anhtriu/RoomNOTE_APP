package com.rikkeisoft.demoroomdatabase.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.rikkeisoft.demoroomdatabase.converter.DateConverter;

import java.io.Serializable;

/**
 * Created by Administrator on 6/22/2017.
 */
// class này sẽ tương ứng với 1 table trong database sử dụng notation @Entity để tạo bảng
@Entity(tableName = "notes")
public class Note implements Serializable {
    @PrimaryKey(autoGenerate = true) // sẽ lấy field gần nhất để làm khóa chính (ở đây là id)
    private int id;
    private String title;
    private String content;
    private int color;
    // vì SQL k lưu đc định dạng kiểu date nên ta cần 1 class DateConverter để chuyển đổi
    @TypeConverters(DateConverter.class)
    private String createDate;
    private String createTime;
    private String pictureListPath;

    public String getPictureListPath() {
        return pictureListPath;
    }

    public void setPictureListPath(String pictureListPath) {
        this.pictureListPath = pictureListPath;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public int getColor() {

        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }


    public Note(int id, String title, String content, String createDate, String createTime, int color, String pictureListPath) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.createDate = createDate;
        this.createTime = createTime;
        this.color = color;
        this.pictureListPath = pictureListPath;
    }
    @Ignore
    public Note(){

    }
}
