package com.rikkeisoft.demoroomdatabase.listener;

import java.text.ParseException;
import java.util.Calendar;

/**
 * Created by Administrator on 29/06/2017.
 */

public interface AlarmListener {
    void processAlarm();
    void setAlarm(Calendar cal, String t, int i) throws ParseException;
}
