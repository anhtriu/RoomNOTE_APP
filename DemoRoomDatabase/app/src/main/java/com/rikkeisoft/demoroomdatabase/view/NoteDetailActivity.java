package com.rikkeisoft.demoroomdatabase.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.rikkeisoft.demoroomdatabase.R;
import com.rikkeisoft.demoroomdatabase.entity.Note;
import com.rikkeisoft.demoroomdatabase.helper.BottomNavigationViewHelper;
import com.rikkeisoft.demoroomdatabase.helper.Util;
import com.rikkeisoft.demoroomdatabase.viewmodel.AddNoteViewModel;
import com.rikkeisoft.demoroomdatabase.viewmodel.NoteListViewModel;
import com.rikkeisoft.demoroomdatabase.viewmodel.NoteViewModel;

import java.util.ArrayList;
import java.util.List;

import static com.rikkeisoft.demoroomdatabase.R.id.showNextNote;
import static com.rikkeisoft.demoroomdatabase.R.id.showPreNote;

public class NoteDetailActivity extends AppCompatActivity {
    ArrayList<Note> arr;
    private BottomNavigationView btnv_option;
    private EditText et_d_title, et_d_content;
    private TextView tv_d_dateTime;
    private Toolbar tb_d_toolbar;
    private int idNote;
    private NoteListViewModel viewModel;
    private NoteViewModel viewModel2;
    private AddNoteViewModel viewModel1;
    private int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_detail);
        init();
        setUpToolbar();
        getNoteInforFromMain();
        processBottomNavigationEvent();
    }

    private void processBottomNavigationEvent() {

        btnv_option.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case showPreNote:
                        showPreNote(item);
                        break;
                    case R.id.shareNote:
                        // shareNote();
                        break;
                    case R.id.deleteNote:
                        deleteNote();
                        break;
                    case showNextNote:
                        showNextNote(item);
                        break;
                }
                return true;
            }
        });
    }

    private void deleteNote() {
        Intent i = getIntent();
        Note n = (Note) i.getSerializableExtra("NOTE");
        viewModel.deleteNote(n);
        finish();
    }

    private void updateNote() {
        Intent i = getIntent();
        Note n = (Note) i.getSerializableExtra("NOTE");
        n.setTitle(et_d_title.getText().toString());
        n.setContent(et_d_content.getText().toString());
        viewModel1.updateNote(n);
        finish();
    }

    private void shareNote() {

    }
    private void showPreNote(MenuItem item) {

        List list = Util.myListNote;
        int i = this.pos - 1;
        this.pos = i;
        Note n = (Note) list.get(i);
        tv_d_dateTime.setText(n.getCreateDate() + " " + n.getCreateTime());
        et_d_title.setText(n.getTitle().toString());
        et_d_content.setText(n.getContent().toString());
        Toast.makeText(this, ""+pos, Toast.LENGTH_SHORT).show();

    }
    private void showNextNote( MenuItem item) {
        List list = Util.myListNote;
        int i = this.pos + 1;
        this.pos = i;
        Note n = (Note) list.get(pos);
        tv_d_dateTime.setText(n.getCreateDate() + " " + n.getCreateTime());
        et_d_title.setText(n.getTitle().toString());
        et_d_content.setText(n.getContent().toString());
        Toast.makeText(this, ""+pos, Toast.LENGTH_SHORT).show();
    }

    private void setUpToolbar() {
        setSupportActionBar(tb_d_toolbar);
        getSupportActionBar().setTitle("Note");
        getSupportActionBar().setIcon(R.drawable.note);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    public void getNoteInforFromMain() {
        Intent i = getIntent();
        Note n = (Note) i.getSerializableExtra("NOTE");
        pos = i.getIntExtra("POS", 0);
        Toast.makeText(this, "" + n.getPictureListPath(), Toast.LENGTH_SHORT).show();
        idNote = n.getId();
        tv_d_dateTime.setText(n.getCreateDate() + " " + n.getCreateTime());
        et_d_title.setText(n.getTitle().toString());
        et_d_content.setText(n.getContent().toString());
        showNote(n.getId());
    }

    private void init() {
        arr = Util.myListNote;
        tb_d_toolbar = (Toolbar) findViewById(R.id.d_toolbar);
        et_d_content = (EditText) findViewById(R.id.d_etContent);
        et_d_title = (EditText) findViewById(R.id.d_etTitle);
        tv_d_dateTime = (TextView) findViewById(R.id.d_datetime);
        btnv_option = (BottomNavigationView) findViewById(R.id.btnv_option);
        BottomNavigationViewHelper.removeShiftMode(btnv_option);
        viewModel = ViewModelProviders.of(this).get(NoteListViewModel.class);
        viewModel1 = ViewModelProviders.of(this).get(AddNoteViewModel.class);
        viewModel2 = ViewModelProviders.of(this).get(NoteViewModel.class);
    }
    private void showNote(int id){
        Note n = viewModel2.getSpecificNoteData(id);
        Toast.makeText(this, ""+n.getId(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail_note, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnd_updatecamera:
                break;
            case R.id.mnd_updateBG:
                break;
            case R.id.mnd_updateNote:
                updateNote();
                break;
            case R.id.addOneNote:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
