package com.rikkeisoft.demoroomdatabase.helper;

import com.rikkeisoft.demoroomdatabase.listener.AlarmListener;

import java.text.ParseException;
import java.util.Calendar;

/**
 * Created by Administrator on 29/06/2017.
 */

public class AlarmHelper {
    private AlarmListener alarmListener;

    public AlarmHelper( AlarmListener alarmListener){
        this.alarmListener = alarmListener;
    }
    public void setAlarm(Calendar m_calender, String s, int i) throws ParseException {
        alarmListener.setAlarm(m_calender,s,i);
    }
}
