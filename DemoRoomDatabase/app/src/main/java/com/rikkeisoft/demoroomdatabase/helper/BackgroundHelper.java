package com.rikkeisoft.demoroomdatabase.helper;

import com.rikkeisoft.demoroomdatabase.listener.ChangeBackgroundListener;

/**
 * Created by Administrator on 29/06/2017.
 */

public class BackgroundHelper {
    ChangeBackgroundListener changeBackgroundListener;
    public BackgroundHelper(ChangeBackgroundListener changeBackgroundListener){
        this.changeBackgroundListener = changeBackgroundListener;
    }
    public void changBG(){
        changeBackgroundListener.changBG();
    }

}
