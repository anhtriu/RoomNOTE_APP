package com.rikkeisoft.demoroomdatabase.view;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.rikkeisoft.demoroomdatabase.R;
import com.rikkeisoft.demoroomdatabase.adapter.RecyclerPictureAdapter;
import com.rikkeisoft.demoroomdatabase.entity.Note;
import com.rikkeisoft.demoroomdatabase.helper.AlarmHelper;
import com.rikkeisoft.demoroomdatabase.helper.BackgroundHelper;
import com.rikkeisoft.demoroomdatabase.helper.CameraHelper;
import com.rikkeisoft.demoroomdatabase.helper.NoteHelper;
import com.rikkeisoft.demoroomdatabase.listener.AlarmListener;
import com.rikkeisoft.demoroomdatabase.listener.CameraListener;
import com.rikkeisoft.demoroomdatabase.listener.ChangeBackgroundListener;
import com.rikkeisoft.demoroomdatabase.listener.CreateNoteListener;
import com.rikkeisoft.demoroomdatabase.receiver.AlarmReceiver;
import com.rikkeisoft.demoroomdatabase.viewmodel.AddNoteViewModel;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AddActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener,AlarmListener, CreateNoteListener, ChangeBackgroundListener, TimePickerDialog.OnTimeSetListener,CameraListener {
    private String m_date = null;
    private String m_alarm = null;
    private String m_dateCreate, m_timCreate;
    private Calendar m_calender;
    private Dialog m_dialog;
    private DatePickerDialog dpDialog;
    private TimePickerDialog tpDialog;
    private AddNoteViewModel m_viewModel;
    private EditText et_title, et_content;
    private int m_color;
    private int nodeId;
    private boolean hasAlarm;
    private Toolbar tb_addToolbar;
    private TextView tv_alarm, tv_datetime;
    private Spinner sp_time, sp_day;
    private ImageView imv_closeSpinner;
    private String[] arrDay, arrTime;
    private ArrayAdapter<String> m_adapterDay;
    private ArrayAdapter<String> m_adapterTime;
    private LinearLayout ln_alarm;
    private long m_nowDate = 0;
    private int list_notes_length=0;
    private RecyclerView rc_listPicture;
    private BackgroundHelper m_BackgroundHelper;
    private AlarmHelper m_AlarmHelper;
    private NoteHelper m_noteHelper;
    private List<Uri> myListPath;
    private RecyclerPictureAdapter m_pictureAdapter;
    private CameraHelper m_cameraHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        init();
        setUpCreateTime();
        setUpToolbar();
        initSpinner();
        event();
    }

    private void setUpCreateTime() {
        m_calender.get(Calendar.YEAR);
        m_calender.get(Calendar.MONTH);
        m_calender.get(Calendar.DAY_OF_MONTH);
        m_calender.get(Calendar.HOUR_OF_DAY);
        m_calender.get(Calendar.MINUTE);
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Date date = m_calender.getTime();
        m_dateCreate = format.format(date);
        SimpleDateFormat format1 = new SimpleDateFormat("HH-MM");
        Date date1 = m_calender.getTime();
        m_timCreate = format1.format(date1);
    }

    private void initSpinner() {
        m_calender.get(Calendar.YEAR);
        m_calender.get(Calendar.MONTH);
        m_calender.get(Calendar.DAY_OF_MONTH);
        m_nowDate = m_calender.getTimeInMillis();
        arrDay = getResources().getStringArray(R.array.day);
        arrTime = getResources().getStringArray(R.array.time);
        m_adapterDay = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, arrDay);
        m_adapterTime = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, arrTime);
        sp_day.setAdapter(m_adapterDay);
        sp_time.setAdapter(m_adapterTime);
        sp_day.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
//                        m_calender.get(Calendar.YEAR);
//                        m_calender.get(Calendar.MONTH);
//                        m_calender.get(Calendar.DAY_OF_MONTH);
//                        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
//                        Date date = m_calender.getTime();
//                        m_date = format.format(date);
                        break;
                    case 1:
                        m_nowDate = m_nowDate + 86400000;
                        break;
                    case 2:
                        break;
                    case 3:
                        dpDialog.show();
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sp_time.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        m_alarm = arrDay[0];
                        break;
                    case 1:
                        m_alarm = arrDay[1];
                        break;
                    case 2:
                        m_alarm = arrDay[2];
                        break;
                    case 3:
                        m_alarm = arrDay[3];
                        break;
                    case 4:
                        tpDialog.show();
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setUpToolbar() {
        setSupportActionBar(tb_addToolbar);
        getSupportActionBar().setTitle("Note");
        getSupportActionBar().setIcon(R.drawable.note);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void event() {
        tv_alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_alarm.setVisibility(View.GONE);
                ln_alarm.setVisibility(View.VISIBLE);
                hasAlarm = true;
            }
        });
        imv_closeSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ln_alarm.setVisibility(View.GONE);
                tv_alarm.setVisibility(View.VISIBLE);
                hasAlarm = false;
            }
        });
    }

    private void init() {
        nodeId = 0;
        setUpColorCard();
        myListPath = new ArrayList<>() ;
        m_cameraHelper = new CameraHelper(this);
        m_AlarmHelper = new AlarmHelper(this);
        m_BackgroundHelper = new BackgroundHelper(this);
        m_noteHelper = new NoteHelper(this);
        m_pictureAdapter = new RecyclerPictureAdapter(getApplicationContext(),myListPath);
        rc_listPicture = (RecyclerView) findViewById(R.id.listPicture);
        rc_listPicture.setHasFixedSize(true);
        rc_listPicture.setLayoutManager(new GridLayoutManager(getApplicationContext(),3));
        rc_listPicture.setAdapter(m_pictureAdapter);
        tv_datetime = (TextView) findViewById(R.id.datetime);
        ln_alarm = (LinearLayout) findViewById(R.id.detailAlarm);
        tb_addToolbar = (Toolbar) findViewById(R.id.add_toolbar);
        sp_day = (Spinner) findViewById(R.id.spnDay);
        sp_time = (Spinner) findViewById(R.id.spnTime);
        tv_alarm = (TextView) findViewById(R.id.tv_alarm);
        imv_closeSpinner = (ImageView) findViewById(R.id.closeSpn);
        m_calender = Calendar.getInstance();
        dpDialog = new DatePickerDialog(this, AddActivity.this,
                m_calender.get(Calendar.YEAR),
                m_calender.get(Calendar.MONTH),
                m_calender.get(Calendar.DAY_OF_MONTH));
        m_viewModel = ViewModelProviders.of(this).get(AddNoteViewModel.class);
        et_content = (EditText) findViewById(R.id.etContent);
        et_title = (EditText) findViewById(R.id.etTitle);
        tpDialog = new TimePickerDialog(this, AddActivity.this,
                m_calender.get(Calendar.HOUR_OF_DAY),
                m_calender.get(Calendar.MINUTE), true);
        hasAlarm = false;
    }
    private void setUpColorCard(){
        m_color =  Color.WHITE;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        m_calender.set(Calendar.YEAR, year);
        m_calender.set(Calendar.MONTH, month);
        m_calender.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Date date = m_calender.getTime();
        m_date = format.format(date);
        arrDay[3] = m_date;
        m_adapterDay.notifyDataSetChanged();
    }


    @Override
    public void onTimeSet(android.widget.TimePicker view, int hourOfDay, int minute) {
        m_calender.set(Calendar.HOUR_OF_DAY, hourOfDay);
        m_calender.set(Calendar.MINUTE, minute);
        SimpleDateFormat format = new SimpleDateFormat("HH-mm");
        Date date = m_calender.getTime();
        m_alarm = format.format(date);
        arrTime[4] = m_alarm;
        m_adapterTime.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mn_camera:
                m_cameraHelper.startcameraAction();
                break;
            case R.id.mn_setBG:
                m_BackgroundHelper.changBG();
                break;
            case R.id.mn_createNote:
                m_noteHelper.makeANewNote();
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
     public void createNote() {
        if (hasAlarm) {
            nodeId = (int) m_viewModel.addNote(new Note(0, et_title.getText().toString(),
                    et_content.getText().toString(), m_dateCreate,
                    m_timCreate, m_color, TextUtils.join(",", this.myListPath)));
                   Toast.makeText(this, "" + nodeId, Toast.LENGTH_SHORT).show();

            try {
                m_AlarmHelper.setAlarm(m_calender, et_title.getText().toString(),++list_notes_length);
                Log.e("Time", "" + m_calender.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        else {
            nodeId = (int) m_viewModel.addNote(new Note(0, et_title.getText().toString(),
                    et_content.getText().toString(), m_dateCreate,
                    m_timCreate, m_color, TextUtils.join(",", this.myListPath)));
            Toast.makeText(this, "" + nodeId, Toast.LENGTH_SHORT).show();
        }
    }


//    private void setAlarm(Calendar cal, String t, int i) throws ParseException {
//        Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
//        intent.putExtra("TEXT", t);
//        PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(),i, intent, 0);
//        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//        alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
//
//    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==109 && resultCode == RESULT_OK){
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            Uri path = getImageUri(getApplicationContext(),bitmap);
            myListPath.add(path);
            m_pictureAdapter.notifyDataSetChanged();

        }
        if(requestCode==195 && resultCode == RESULT_OK){
            Uri path = data.getData();
            myListPath.add(path);
            m_pictureAdapter.notifyDataSetChanged();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    @Override
    public void startProcess() {
        CharSequence options[] = new CharSequence[] {"Take Photos", "Open Galery"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle("Select your option:");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case 1:
                        m_cameraHelper.processCamera();
                        break;
                    case 0:
                        m_cameraHelper.processGalery();
                        break;

                }
            }
        });
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //the user clicked on Cancel
            }
        });
        builder.show();
    }

    @Override
    public void processCam() {
        Intent galery = new Intent(Intent.ACTION_GET_CONTENT);
        galery.setType("image/*");
        startActivityForResult(galery,195);
        rc_listPicture.setVisibility(View.VISIBLE);
    }

    @Override
    public void processGa() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent,109);
        rc_listPicture.setVisibility(View.VISIBLE);
    }

    @Override
    public void changBG() {
        m_dialog = new Dialog(this);
        m_dialog.setContentView(R.layout.dialog_choose_bg);
        m_dialog.setTitle("Choose a background");
        ImageView imv_first = (ImageView) m_dialog.findViewById(R.id.imv_first);
        ImageView imv_second = (ImageView) m_dialog.findViewById(R.id.imv_second);
        ImageView imv_third = (ImageView) m_dialog.findViewById(R.id.imv_third);
        ImageView imv_fouth = (ImageView) m_dialog.findViewById(R.id.imv_fouth);
        m_dialog.show();
        imv_first.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_color = Color.CYAN;
                m_dialog.dismiss();

            }
        });
        imv_second.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_color = Color.BLUE;
                m_dialog.dismiss();

            }
        });
        imv_third.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_color = Color.YELLOW;
                m_dialog.dismiss();

            }
        });
        imv_fouth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_color = Color.RED;
                m_dialog.dismiss();

            }
        });
    }

    @Override
    public void processAlarm() {

    }

    @Override
    public void setAlarm(Calendar cal, String t, int i) throws ParseException {
        Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
        intent.putExtra("TEXT", t);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(),i, intent, 0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
    }
}
