package com.rikkeisoft.demoroomdatabase.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Administrator on 6/23/2017.
 */

@Entity(tableName = "pictures", foreignKeys = {
        @ForeignKey(entity = Note.class,
                parentColumns = "id",
                childColumns = "noteId",
                onDelete = ForeignKey.CASCADE)}, indices = {
        @Index(value = "noteId")
})
public class Picture  {
    @PrimaryKey(autoGenerate = true)
    private int pictureId;
    private String picturePath;
    private int nodeId;

    public int getPictureId() {
        return pictureId;
    }

    public void setPictureId(int pictureId) {
        this.pictureId = pictureId;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }

    public int getNodeId() {
        return nodeId;
    }

    public void setNodeId(int nodeId) {
        this.nodeId = nodeId;
    }

    public Picture(int pictureId, String picturePath, int nodeId) {
        this.pictureId = pictureId;
        this.picturePath = picturePath;
        this.nodeId = nodeId;
    }

    public Picture() {
    }
}
