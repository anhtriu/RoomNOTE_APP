package com.rikkeisoft.demoroomdatabase.converter;

import android.arch.persistence.room.TypeConverter;

import java.util.Date;

/**
 * Created by Administrator on 6/22/2017.
 */

public class DateConverter {
    @TypeConverter
    public static Date toDate(Long timestamp) {
        return timestamp == null ? null : new Date(timestamp);
    }

    @TypeConverter
    public static Long toTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }
}
