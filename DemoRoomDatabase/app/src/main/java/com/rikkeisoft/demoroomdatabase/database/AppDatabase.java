package com.rikkeisoft.demoroomdatabase.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.rikkeisoft.demoroomdatabase.dao.NoteDAO;
import com.rikkeisoft.demoroomdatabase.entity.Note;

/**
 * Created by Administrator on 6/22/2017.
 */
// tạo 1 abstract class để tạo database abstract
@Database(entities = {Note.class},version = 1)
public abstract  class AppDatabase extends RoomDatabase{
 private static AppDatabase INSTANCE;
    public static AppDatabase getDatabase(Context context){
        if(INSTANCE==null){
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),AppDatabase.class,"note_db").build();
        }
        return INSTANCE;
    }
    public abstract NoteDAO dataModel();

}


