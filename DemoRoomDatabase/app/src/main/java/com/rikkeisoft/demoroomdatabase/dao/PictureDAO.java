package com.rikkeisoft.demoroomdatabase.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.Update;

import com.rikkeisoft.demoroomdatabase.converter.DateConverter;
import com.rikkeisoft.demoroomdatabase.entity.Picture;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by Administrator on 28/06/2017.
 */
@Dao
@TypeConverters(DateConverter.class)
    public interface PictureDAO {
        @Query("select * from pictures where id = :noteId")
        LiveData<List<Picture>> getAllPicture(int id);
        @Insert(onConflict = REPLACE)
        long insertPicture(Picture picture);
        @Update
        void update(Picture picture);
        @Delete
        void deleteNote(Picture picture);
}
