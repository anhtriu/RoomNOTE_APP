package com.rikkeisoft.demoroomdatabase.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rikkeisoft.demoroomdatabase.R;
import com.rikkeisoft.demoroomdatabase.entity.Note;
import com.rikkeisoft.demoroomdatabase.helper.Util;

import java.util.ArrayList;
import java.util.List;

import static com.rikkeisoft.demoroomdatabase.R.id.card_item;

/**
 * Created by Administrator on 6/22/2017.
 */

public class RecylerNoteAdapter extends RecyclerView.Adapter<RecylerNoteAdapter.NoteViewHolder> {
    private List<Note> listNote;
    private Context context;
    private View.OnClickListener onClickListener;

    public RecylerNoteAdapter(Context context, List<Note> listNote, View.OnClickListener onClickListener){
        this.context = context;
        this.listNote = listNote;
        this.onClickListener = onClickListener;
    }
    @Override
    public NoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_note,null);
        NoteViewHolder i = new NoteViewHolder(v);
        return i;
    }

    @Override
    public void onBindViewHolder(NoteViewHolder holder, int position) {
        Note m_note = listNote.get(position);
        holder.tv_noteTitle.setText(m_note.getTitle());
        holder.tv_noteContent.setText(m_note.getContent());
        holder.tv_notedate.setText(m_note.getCreateDate().toString()+" "+m_note.getCreateTime().toString());
        holder.cd_cardItem.setCardBackgroundColor(m_note.getColor());
        holder.itemView.setTag(m_note);
        holder.itemView.setTag(R.string.pos,position);
        holder.itemView.setOnClickListener(onClickListener);
    }

    @Override
    public int getItemCount() {
        return listNote.size();
    }
    public void addNotesItem(List<Note> listNote){
        this.listNote = listNote;
        Util.myListNote = (ArrayList<Note>) listNote;
        notifyDataSetChanged();
    }

    public class NoteViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_noteTitle;
        TextView tv_noteContent;
        TextView tv_notedate;
        CardView cd_cardItem;
        public NoteViewHolder(View itemView) {
            super(itemView);
            tv_noteTitle = (TextView) itemView.findViewById(R.id.title);
            tv_noteContent = (TextView) itemView.findViewById(R.id.content);
            tv_notedate = (TextView) itemView.findViewById(R.id.note_date);
            cd_cardItem = (CardView) itemView.findViewById(card_item);
        }

        @Override
        public void onClick(View v) {
            int postion = getAdapterPosition();
        }
    }
}
