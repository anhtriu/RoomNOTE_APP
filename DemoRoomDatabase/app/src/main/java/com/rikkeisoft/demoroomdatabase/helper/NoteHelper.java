package com.rikkeisoft.demoroomdatabase.helper;

import com.rikkeisoft.demoroomdatabase.listener.CreateNoteListener;

/**
 * Created by Administrator on 29/06/2017.
 */

public class NoteHelper  {
    private CreateNoteListener createNoteListener;
    public NoteHelper(CreateNoteListener createNoteListener){
        this.createNoteListener = createNoteListener;
    }
    public void makeANewNote(){
        createNoteListener.createNote();
    }
}
